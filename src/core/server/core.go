package server

import (
	"src/data/database"
	"src/utility"
)

//Все добавления делаем в отдельных ветках
//Однако они должны быть смысловыми, в случае если есть возможность исправить
//чужую ветку, то лучше спросите у автора, основная ветка для тестов разработки - release

var errorCore = "Core module error#"

var core Core

//Core - is a storage structure for Core units
type Core struct {
	conf *utility.Configuration //Object configuration data from file
	proc *HTTPProcess           //Object server
	db   *database.Database     //Object database connect
}

//InitCore - initialization core
func InitCore(path string) error {
	conf, err := utility.LoadFile(path) //Read config file
	if err != nil {
		return err
	}
	if !conf.IsValid() {
		return utility.ErrorCreate(errorCore, "InitCore.- In configuration file attend empty fields... ")
	}
	core = Core{conf, InitServer(), database.DBInstance(conf.ConnectStrPg)}
	return nil
}

//Start - starting core
func Start(err chan<- string, act chan interface{}) error {
	if errDb := core.db.InitDatabaseConnections(); errDb != nil {
		err <- errDb.Error()
		return errDb
	}
	core.Handlers(err)
	if errProc := core.proc.StartProcess(core.conf); errProc != nil {
		err <- errProc.Error()
		return errProc
	}
	return nil
}
