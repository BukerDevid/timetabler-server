package utility

type AnswerFunction struct {
	Error             error
	Data              []byte
	TokenSession      string // CUSTOM Token user
	EncryptionSession string // CUSTOM Session key (in WEB cookie)
	Key               string
}

func (answ *AnswerFunction) GetError() string {
	return answ.Error.Error()
}

var KEYS = map[int]string{
	1: "Invalid login",                   // HTTP 401
	2: "Invalid pwd",                     // HTTP 401
	3: "Invalid GetToken",                // HTTP 420
	4: "Cannot register",                 // HTTP 400?
	5: "JSON Marshalling/Decoding error", // HTTP 500
	6: "DB request error",                // HTTP 452
	7: "DB insert error",                 // HTTP 453
	8: "DB update error",                 // HTTP 454

}

func CreateData(value interface{}, key interface{}) AnswerFunction {
	var data AnswerFunction
	if f, ok := value.(error); ok {
		data.Error = f
	}
	if f, ok := value.([]byte); ok {
		data.Data = f
	}
	if f, ok := key.(int); ok {
		data.Key = KEYS[f]
	}
	return data
}
