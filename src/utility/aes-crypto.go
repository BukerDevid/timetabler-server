package utility

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
)

const BlockSize = 16

func GenKeyForEncriptors(word string) string {
	b := []byte(word)
	hash := md5.Sum(b)
	return fmt.Sprintf("%x", hash)
}

//encryptAES - реализация шифрования на AES
func encryptAES(key []byte, plaintext []byte) (string, error) {
	cipherAES, err := aes.NewCipher(key)
	if err != nil {
		return "", ErrorCreate("ENSCRYPTION AES", err.Error())
	}
	if len(plaintext)%aes.BlockSize != 0 {
		return "", ErrorCreate("ENSCRYPTION AES", err.Error())
	}
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", ErrorCreate("ENSCRYPTION AES", err.Error())
	}
	mode := cipher.NewCBCEncrypter(cipherAES, iv)
	mode.CryptBlocks(ciphertext[aes.BlockSize:], []byte(plaintext))
	return fmt.Sprintf("%x", ciphertext), nil
}

//decryptAES - реализация расшифровки с AES
func decryptAES(key_word, ciphertext []byte) ([]byte, error) {
	block, err := aes.NewCipher(key_word)
	if err != nil {
		return nil, ErrorCreate("DECRYPT", err.Error())
	}
	if len(ciphertext) < aes.BlockSize {
		return nil, ErrorCreate("DECRYPT", err.Error())
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	if len(ciphertext)%aes.BlockSize != 0 {
		return nil, ErrorCreate("DECRYPT", err.Error())
	}
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(ciphertext, ciphertext)
	return ciphertext, nil
}

//Encription - шфирования токена в ключ сессии.
func Encription(key_word, token string) (string, error) {
	if len(token) != 128 {
		return "", ErrorCreate("ENSCRIPTION", fmt.Sprintf("filed length token %s", token))
	}
	if len(key_word)%BlockSize != 0 {
		return "", ErrorCreate("ENSCRIPTION", fmt.Sprintf("filed length key_word %s", key_word))
	}
	key, err := hex.DecodeString(key_word)
	if err != nil {
		return "", ErrorCreate("ENSCRIPTION", err.Error())
	}
	enscription_session, err := encryptAES(key, []byte(token))
	if err != nil {
		return "", ErrorCreate("ENSCRIPTION", err.Error())
	}
	return enscription_session, nil
}

//Decrypt - расшифровка ключа сессии в токен
func Decrypt(key_word, encryption_session string) (string, error) {
	key, err := hex.DecodeString(key_word)
	if err != nil {
		return "", ErrorCreate("DECRYPT", err.Error())
	}
	ciphertext, err := hex.DecodeString(encryption_session)
	if err != nil {
		return "", ErrorCreate("DECRYPT", err.Error())
	}
	ciphertext, err = decryptAES(key, ciphertext)
	if err != nil {
		return "", ErrorCreate("DECRYPT", err.Error())
	}
	return string(ciphertext), nil
}
