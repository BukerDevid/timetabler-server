package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"src/core/server"
	"src/utility"
)

var pathConfiguration = "config.json"
var pathLogFile = "logerr.log"
var quit = make(chan os.Signal)
var errChan = make(chan string)
var actionChan = make(chan interface{})

func main() {
	err := utility.CreateLog(pathLogFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if errCore := server.InitCore(pathConfiguration); errCore != nil {
		log.Fatal(errCore) // If it is called then the program is closed
	}
	signal.Notify(quit, syscall.SIGKILL) //Notify if the OS killing this program
	fmt.Println("OnStudy Server")
	//Service goroutine
	go func() {
		if errSt := server.Start(errChan, actionChan); errSt != nil {
			fmt.Println(errSt)
			os.Exit(1)
		}
	}()
	//Log goroutine
	go func() {
		for {
			select {
			case val := <-errChan:
				utility.WriteLog(val)
			case <-quit:
				return
			}
		}
	}()
	<-quit
}
