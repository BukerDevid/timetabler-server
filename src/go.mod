module src

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jackc/pgx/v4 v4.13.0
	github.com/valyala/fasthttp v1.28.0
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
)
