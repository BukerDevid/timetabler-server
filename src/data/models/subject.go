package models

//ModelSubjectLessonList - список предметов
type ModelSubjectLessonList struct {
	Subjects []ModelSubjectLesson `json:"subjects"`
}

//ModelSubjectLesson - сущность предмета (учебного предмета)
type ModelSubjectLesson struct {
	ID          int             `json:"id"`
	YearCreate  string          `json:"year-create"`
	Code        string          `json:"code"`
	Name        string          `json:"name"`
	Course      int             `json:"course"`
	Semester    int             `json:"semestr"`
	Timing      ModelTiming     `json:"timing"`
	Speciality  ModelSpeciality `json:"speciality"`
	Attestation string          `json:"attestation"`
}

//ModelChapterLesson -сущность глава (основная тема)
type ModelChapterLesson struct {
	ID          int    `json:"id"`
	NameChapter string `json:"name"`
}

//ModelTopic - сущность тема
type ModelTopic struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	Time       int    `json:"hour"`
	TypeLesson string `json:"tyle-lesson"`
}

//ModelTiming - мущность время предмета
type ModelTiming struct {
	ID           int `json:"id"`
	Maximal      int `json:"maximal"`
	Selfwork     int `json:"selfwork"`
	AllTime      int `json:"all-time"`
	Teory        int `json:"teory"`
	Laboratory   int `json:"laboratory"`
	Practice     int `json:"practice"`
	CourseWork   int `json:"course-work"`
	Consultation int `json:"consultation"`
	Exam         int `json:"exam"`
}
