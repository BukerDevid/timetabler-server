package models

//ModelCource - модель курсов
type ModelCource struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	AuthorID    int    `json:"author"`
	Description string `json:"description"`
	DateCreate  string `json:"date"`
}

//ModelListCources - список курсов
type ModelListCources struct {
	List []ModelCource `json:"list"`
}
