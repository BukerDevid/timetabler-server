package models

//ModelListUser - Модель списка пользователей
type ModelListUser struct {
	User []ModelUser `json:"user"`
}

//ModelUser - сущность пользователь
type ModelUser struct {
	ID         int           `json:"id"`
	Firstname  string        `json:"firstname"`
	Secondname string        `json:"secondname"`
	Middlename string        `json:"middlename"`
	Birthday   string        `json:"birthday"`
	Phone      string        `json:"phone"`
	Email      string        `json:"emailaddres"`
	Status     string        `json:"status"`
	Teacher    ModelTeacher  `json:"teacher"`
	Student    ModelStudent  `json:"student"`
	Enrollee   ModelEnrollee `json:"enrollee"`
}

//ModelTeacher - модель уникальных параметров сущности преподаватель
type ModelTeacher struct {
}

//ModelStudent - модель уникальных параметров сущности студент
type ModelStudent struct {
}

//ModelEnrollee - модель уникальных параметров сущности студент
type ModelEnrollee struct {
}
