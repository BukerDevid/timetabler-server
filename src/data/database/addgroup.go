package database

import (
	"context"
	"fmt"
	"src/utility"
)

// Хочу добавть какие правки с БД
// Хочу провести запрос на слияние... нужно проверить конфликты...

//addGroup - добавление группы
func (db *Database) addGroup(
	number_group string, date_formation string, status_group string, type_learning int,
	speciality_id int) (bool, error) {
	// tTime := time.Now()
	//Регистрация пользователя
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return false, utility.ErrorCreate("REG", err.Error())
	}
	//Начало транзакции
	transaction, err := connToDatabase.Begin(context.Background())
	defer transaction.Rollback(context.Background())
	// TODO: довести код до рабочего состояния
	//проверка на существование группы
	// if err := transaction.QueryRow(
	// 	context.Background(), "SELECT number_group FROM groups where number_group = ($1)", number_group); err != nil {
	// 	return false, utility.ErrorCreate("REG", err.)
	// }
	// //Формат даты
	// date_formation = tTime.Format(date_formation)
	// //Пишем в бд
	// var groupID int
	// fmt.Println(number_group, date_formation, status_group, type_learning, speciality_id)
	// if err := transaction.QueryRow(
	// 	context.Background(),
	// 	"INSERT INTO groups (number_group, date_formation, status_group, type_learning, speciality_id) VALUES ($1,$2,$3,$4,$5) RETURNING id",
	// 	number_group, date_formation, status_group, type_learning, speciality_id).Scan(&groupID); err != nil {
	// 	return false, utility.ErrorCreate("REG", err.Error())
	// }
	// transaction.Commit(context.Background())
	return true, nil
}

//delGroup - для удаление группы.
func (db *Database) delGroup(number_group string) (bool, error) {
	//Регистрация пользователя
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return false, utility.ErrorCreate("REG", err.Error())
	}
	//Начало транзакции
	transaction, err := connToDatabase.Begin(context.Background())
	defer transaction.Rollback(context.Background())
	var groupID int
	//проверка на существование группы
	if err := transaction.QueryRow(
		context.Background(), "SELECT number_group FROM groups where number_group = ($1)", number_group).Scan(&groupID); err != nil {
		return false, utility.ErrorCreate("REG", err.Error())
	}
	fmt.Println(groupID)
	if err := transaction.QueryRow(
		context.Background(),
		"DELETE FROM groups where id = (&1)", groupID); err != nil {
		// return false, utility.ErrorCreate("REG", err.Error())
	}
	transaction.Commit(context.Background())
	return true, nil
}
