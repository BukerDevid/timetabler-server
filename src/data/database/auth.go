package database

import (
	"context"
	"fmt"

	"src/utility"
)

//AuthUserCheckLoginInDatabase - Проверка логина (телефона) в базе данных
func (db *Database) AuthUserCheckLoginInDatabase(phone string) (int, bool, error) {
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return 0, false, utility.ErrorCreate("AUTH LOGIN", err.Error())
	}
	phone = utility.AdaptivePhone(phone)
	fmt.Println(phone)
	var scanPhone string
	var id int
	if err := connToDatabase.QueryRow(context.Background(), "SELECT id, phone FROM users WHERE phone = $1", phone).Scan(&id, &scanPhone); err != nil {
		return 0, false, utility.ErrorCreate("AUTH LOGIN", err.Error())
	}
	return id, true, nil
}

//AuthUserCheckPwdInDatabase - Проверка пароля в базе данных
func (db *Database) AuthUserCheckPwdInDatabase(phone string, pwd string) (bool, error) {
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return false, utility.ErrorCreate("AUTH PWD", err.Error())
	}
	phone = utility.AdaptivePhone(phone)
	var scanPwd string
	if err := connToDatabase.QueryRow(context.Background(), "SELECT pwd FROM users WHERE phone = $1 AND pwd = $2", phone, pwd).Scan(&scanPwd); err != nil {
		return false, utility.ErrorCreate("AUTH PWD", err.Error())
	}
	return true, nil
}

//GetModeOfUserInDatabase - Выгрузка режима пользователя
func (db *Database) GetModeOfUserInDatabase(userID int) (string, error) {
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return "", utility.ErrorCreate("GET MODE", err.Error())
	}
	// phone = utility.AdaptivePhone(phone)
	// TODO: перенести определение статусв в функцию postgresql
	// var admin, student, teacher interface{}
	// if err := connToDatabase.QueryRow(context.Background(), "SELECT ad, st, te FROM (SELECT CASE WHEN COUNT(*) != 0 THEN TRUE ELSE FALSE END FROM admins WHERE admins.id = $1) AS ad, (SELECT CASE WHEN COUNT(*) != 0 THEN TRUE ELSE FALSE END FROM students WHERE students.id = $1) AS st, (SELECT CASE WHEN COUNT(*) != 0 THEN TRUE ELSE FALSE END FROM teachers WHERE teachers.id = $1) AS te",
	// 	userID).Scan(&admin, &student, &teacher); err != nil {
	// 	return "", utility.ErrorCreate("GET MODE SELECT", err.Error())
	// }
	// if student.(bool) {
	// 	return "student", nil
	// }
	// if teacher.(bool) {
	// 	return "teacher", nil
	// }
	// if admin.(bool) {
	// 	return "admin", nil
	// }
	return "enrelle", nil
}
