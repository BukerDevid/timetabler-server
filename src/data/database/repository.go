package database

import (
	"encoding/json"
	"fmt"
	"net/http"

	// "strconv"

	"src/data/models"
	"src/utility"
)

/*
Repository
*/

//Authorize - user authorization data
func Authorize(root *Database, phone *string, pwd *string, web *string, ip *string) utility.AnswerFunction { //([]byte, string, error)
	var token models.TokenModel
	var err error
	var id int
	*phone = utility.AdaptivePhone(*phone)
	//Check login
	id, ok, err := root.AuthUserCheckLoginInDatabase(*phone)
	if !ok {
		return utility.CreateData(err, 1)
	}
	//Check pwd
	if ok, err := root.AuthUserCheckPwdInDatabase(*phone, *pwd); !ok {
		return utility.CreateData(err, 2)
	}
	//Generate token
	token.Token, err = root.GenTokenForUserInDatabase(id, *ip)
	if err != nil {
		return utility.CreateData(err, 3)
	}
	//Check mode user
	token.Mode, err = root.GetModeOfUserInDatabase(id)
	if err != nil {
		return utility.CreateData(err, 4)
	}
	//Marshaling
	data, err := json.Marshal(token)
	if err != nil {
		return utility.CreateData(utility.ErrorCreate("MARSHAL AUTH DATA", err.Error()), 5)
	}
	if *web == "t" {
		enscryption_session, err := root.GenKeySessionForUserInDatabase(token.Token, *ip)
		if err != nil {
			return utility.CreateData(err, 3)
		}
		res := utility.CreateData(err, 3)
		res.EncryptionSession = enscryption_session
		return res
	}
	//Send token
	return utility.CreateData(data, nil)
}

//RegisterUser - processes user registration data
func RegisterUser(root *Database, r *http.Request, web *string) utility.AnswerFunction {
	var token models.TokenModel
	var user models.ModelRegUser
	var err error
	var id int
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		return utility.CreateData(err, 5)
	}
	//Check login
	id, _, err = root.AuthUserCheckLoginInDatabase(user.Phone)
	if err == nil {
		return utility.CreateData(err, 1)
	}
	//Registration user
	if ok, err := root.RegUserInDatabase(user.Firstname, user.Lastname, user.Middlename, user.Birthday, user.Phone, user.Emailaddres, user.Pwd); !ok {
		return utility.CreateData(err, 4)
	}
	//Generate token
	token.Token, err = root.GenTokenForUserInDatabase(id, r.RemoteAddr)
	if err != nil {
		return utility.CreateData(err, 3)
	}
	fmt.Println("Token - " + token.Token)
	//Marshaling
	data, err := json.Marshal(token)
	if err != nil {
		return utility.CreateData(utility.ErrorCreate("MARSHAL AUTH DATA", err.Error()), 5)

	}
	if *web == "t" {
		enscryption_session, err := root.GenKeySessionForUserInDatabase(token.Token, r.RemoteAddr)
		if err != nil {
			return utility.CreateData(err, 3)
		}
		res := utility.CreateData(err, 3)
		res.EncryptionSession = enscryption_session
		return res

	}
	return utility.CreateData(data, nil)
}

//GetListUsers - Выгрузка пользователей, по фильтрам
func GetListUsers(root *Database, token, filters string) utility.AnswerFunction {
	//Get List
	users, err := root.GetListUsers(filters)
	if err != nil {
		return utility.CreateData(err, 6)
	}
	//Marshaling
	data, err := json.Marshal(users)
	if err != nil {
		return utility.CreateData(utility.ErrorCreate("MARSHAL AUTH DATA", err.Error()), 5)
	}
	return utility.CreateData(data, nil)
}

// //GetListNews - Выгрузка новостей по идентификатору пользователя
// func GetListNews(root *Database, token string) utility.AnswerFunction {
// 	//Get ID
// 	id, err := root.CheckTokenForUserInDatabase(token)
// 	if err != nil {
// 		return utility.CreateData(err, 3)
// 	}
// 	//Get List
// 	// TODO: Требуется реализовать в БД
// 	news, err := root.GetListNews(id)
// 	if err != nil {
// 		return utility.CreateData(err, 6)
// 	}
// 	//Marshalling
// 	data, err := json.Marshal(news)
// 	if err != nil {
// 		return utility.CreateData(utility.ErrorCreate("MARSHAL AUTH DATA", err.Error()), 5)
// 	}
// 	return utility.CreateData(data, nil)
// }

// //GetListSchedule - Выгрузка расписания по идентификатору пользователя
// func GetListSchedule(root *Database, token string) utility.AnswerFunction {
// 	//Get ID
// 	id, err := root.CheckTokenForUserInDatabase(token)
// 	if err != nil {
// 		return utility.CreateData(err, 3)
// 	}
// 	//Get List
// 	schedule, err := root.GetListSchedule(id)
// 	if err != nil {
// 		return utility.CreateData(err, 6)
// 	}
// 	//Marshalling
// 	data, err := json.Marshal(schedule)
// 	if err != nil {
// 		return utility.CreateData(utility.ErrorCreate("MARSHAL AUTH DATA", err.Error()), 5)
// 	}
// 	return utility.CreateData(data, nil)
// }

// //UploadNews - загрузка новостей
// func UploadNews(root *Database, r *http.Request, token string) utility.AnswerFunction {
// 	var news models.ModelNews
// 	//Check token
// 	_, err := root.CheckTokenForUserInDatabase(token)
// 	if err != nil {
// 		return utility.CreateData(err, 3)
// 	}
// 	//Get the news model
// 	err = json.NewDecoder(r.Body).Decode(&news)
// 	if err != nil {
// 		return utility.CreateData(err, 5)
// 	}
// 	//Send the news model to the DB
// 	if ok, err := root.AddNewsInDatabase(news.From, news.Date, news.Content); !ok { //TODO: изменить поля сущности на те, которые будут в БД
// 		return utility.CreateData(err, 7)
// 	} else {
// 		return utility.CreateData(ok, nil)
// 	}
// }

// //UploadSchedule - загрузка расписания
// func UploadSchedule(root *Database, r *http.Request, token string) utility.AnswerFunction {
// 	var sched models.ModelSchedule
// 	//Check token
// 	_, err := root.CheckTokenForUserInDatabase(token)
// 	if err != nil {
// 		return utility.CreateData(err, 3)
// 	}
// 	//Get the schedule model
// 	err = json.NewDecoder(r.Body).Decode(&sched)
// 	if err != nil {
// 		return utility.CreateData(err, 5)
// 	}
// 	//Send the schedule model to the DB
// 	if ok, err := root.AddScheduleInDatabase(sched.SubjectName, sched.Date, sched.Teacher); !ok { //TODO: изменить поля сущности на те, которые будут в БД
// 		return utility.CreateData(err, 7)
// 	} else {
// 		return utility.CreateData(ok, nil)
// 	}
// }

// //UpdateNews - обновление новостей
// func UpdateNews(root *Database, r *http.Request, token string) utility.AnswerFunction {
// 	var news models.ModelNews
// 	//Check token
// 	_, err := root.CheckTokenForUserInDatabase(token)
// 	if err != nil {
// 		return utility.CreateData(err, 3)
// 	}
// 	//Get the news model
// 	err = json.NewDecoder(r.Body).Decode(&news)
// 	if err != nil {
// 		return utility.CreateData(err, 5)
// 	}
// 	//Send the news model to the DB
// 	if ok, err := root.UpdateNewsInDatabase(news.From, news.Date, news.Content); !ok { //TODO: изменить поля сущности на те, которые будут в БД
// 		return utility.CreateData(err, 8)
// 	} else {
// 		return utility.CreateData(ok, nil)
// 	}
// }

// //UpdateSchedule - обновление расписания
// func UpdateSchedule(root *Database, r *http.Request, token string) utility.AnswerFunction {
// 	var sched models.ModelSchedule
// 	//Check token
// 	_, err := root.CheckTokenForUserInDatabase(token)
// 	if err != nil {
// 		return utility.CreateData(err, 3)
// 	}
// 	//Get the schedule model
// 	err = json.NewDecoder(r.Body).Decode(&sched)
// 	if err != nil {
// 		return utility.CreateData(err, 5)
// 	}
// 	//Send the schedule model to the DB
// 	if ok, err := root.UpdateScheduleInDatabase(sched.SubjectName, sched.Date, sched.Teacher); !ok { //TODO: изменить поля сущности на те, которые будут в БД
// 		return utility.CreateData(err, 8)
// 	} else {
// 		return utility.CreateData(ok, nil)
// 	}
// }

// //BlockUser - Блокировка пользователя
// func BlockUser(root *Database, token string) utility.AnswerFunction {
// 	//Get status
// 	status, err := root.CheckBlockStatus(token)
// 	if err != nil {
// 		return utility.CreateData(err, 6)
// 	}
// 	if !status {
// 		_, err = root.BlockUser(token)
// 		if err != nil {
// 			return utility.CreateData(err, 7)
// 		}
// 	}
// 	return utility.CreateData(!status, nil)
// }

// //UnblockUser - Разблокировка пользователя
// func UnblockUser(root *Database, token string) utility.AnswerFunction {
// 	//Get status
// 	status, err := root.CheckBlockStatus(token)
// 	if err != nil {
// 		return utility.CreateData(err, 6)
// 	}
// 	if status {
// 		_, err = root.UnblockUser(token)
// 		if err != nil {
// 			return utility.CreateData(err, 7)
// 		}
// 	}
// 	return utility.CreateData(!status, nil)
// }

// //CheckToken - Проверяет токен на действительность
// func CheckToken(root *Database, token string) (int, error) {
// 	return root.CheckTokenForUserInDatabase(token)
// }

// //CheckKeySession - расшифровка сессии
// func CheckKeySession(root *Database, encryption_session string, r *http.Request) (string, error) {
// 	return root.GetTokenFromKeySessionInDatabase(encryption_session, r.RemoteAddr)
// }

//CheckPhone -
func CheckPhone(root *Database, phone string) error {
	if _, ok, err := root.AuthUserCheckLoginInDatabase(phone); !ok {
		return err
	}
	return nil
}

// //LoadListCources - Список курсов, всех и личные курсы
// func LoadListCources(root *Database, token string, filter string) ([]byte, error) {
// 	userID, err := root.CheckTokenForUserInDatabase(token)
// 	if err != nil {
// 		return nil, utility.ErrorCreate("LOAD COURCE", "Недостаточно прав для доступа")
// 	}
// 	list, err := root.GetListCources(filter, userID)
// 	if err != nil {
// 		return nil, utility.ErrorCreate("LOAD COURCE", "Недостаточно прав для доступа")
// 	}
// 	data, err := json.Marshal(list)
// 	if err != nil {
// 		return nil, utility.ErrorCreate("LOAD COURCE", "Сервис временно недоступен")
// 	}
// 	return data, nil
// }

// //LoadListChapters - Список курсов, всех и личные курсы
// func LoadListChapters(root *Database, token string, cource string) ([]byte, error) {
// 	_, err := root.CheckTokenForUserInDatabase(token)
// 	if err != nil {
// 		return nil, utility.ErrorCreate("LOAD CHAPTER", "Недостаточно прав для доступа")
// 	}
// 	id, err := strconv.Atoi(cource)
// 	if err != nil {
// 		return nil, utility.ErrorCreate("LOAD CHAPTER", "Некорректно указан курс, исправьте ошибку и повторите ошибку!")
// 	}
// 	list, err := root.GetListChapters(id)
// 	if err != nil {
// 		return nil, utility.ErrorCreate("LOAD COURCE", err.Error())
// 	}
// 	data, err := json.Marshal(list)
// 	if err != nil {
// 		return nil, utility.ErrorCreate("LOAD COURCE", "Ошибка представления данных")
// 	}
// 	return data, nil
// }
