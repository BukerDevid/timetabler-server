package database

/*
Packet database for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru
*/

import (
	"context"

	"src/utility"

	pgxp "github.com/jackc/pgx/v4/pgxpool"
)

var (
	errorPQX = "Database driver PGX# "
)

/*
Database is type for interaction with local database. She has methods initialization, opening connection and closing connection.
Fileds:
connectionStrPG string - config URI string for connection to PostgreSQL database
dbConnectPool   *pgx.ConnPool - keep pool connection to PostgreSQL database, for keep one connects use pool.Acquire() (conn, error)
*/
type Database struct {
	connectionStrPG string
	dbConnectPool   *pgxp.Pool
}

//DBInstance - return of instance database
func DBInstance(connStrPG string) *Database {
	return &Database{connectionStrPG: connStrPG}
}

//InitDatabaseConnections - opening connection to database
func (db *Database) InitDatabaseConnections() error {
	confConn, err := pgxp.ParseConfig(db.connectionStrPG) // Example: user=jack password=secret host=host1,host2,host3 port=5432,5433,5434 dbname=mydb sslmode=verify-ca
	if err != nil {                                       // https://www.postgresql.org/docs/11/libpq-connect.html#LIBPQ-MULTIPLE-HOSTS
		return utility.ErrorHandler(errorPQX+" ParseURI ", err)
	}
	db.dbConnectPool, err = pgxp.ConnectConfig(context.Background(), confConn)
	if err != nil {
		return utility.ErrorHandler(errorPQX+" NewConnPool ", err)
	}
	return nil
}

//...
