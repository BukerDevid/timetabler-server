package database

import (
	"context"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"math/rand"
	"src/utility"
	"time"
)

//takeSha256Tok - функция для генерации токена 64 байта
func takeSha256Tok(word string) string {
	b := make([]byte, 8)
	b = []byte(word)
	hash := sha256.Sum256(b)
	return fmt.Sprintf("%x", hash)
}

//takeSha256Sol - соль для токена
func takeSha256Sol() string {
	actual := time.Now().Unix()
	actual = actual + rand.Int63n(33)
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(actual))
	hash := sha256.Sum256(b)
	return fmt.Sprintf("%x", hash)
}

//GenTokenForUserInDatabase - Генерация токена для пользователя с записью в БД (присваевается к id)
func (db *Database) GenTokenForUserInDatabase(userID int, ip string) (string, error) {
	//Take connect form database
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return "", utility.ErrorCreate("GEN TOKEN", err.Error())
	}
	token := fmt.Sprintf("%v%v", takeSha256Tok(ip), takeSha256Sol())
	if _, err := connToDatabase.Exec(context.Background(), "INSERT INTO token(token,users_id,ip,device) VALUES($1,$2,$3,'unknow')", token, userID, ip); err != nil {
		return "", utility.ErrorCreate("GEN TOKEN", err.Error())
	}
	return token, nil
}

//CheckTokenForUserInDatabase - Генерация токена для пользователя с записью в БД (присваевается к ip)
func (db *Database) CheckTokenForUserInDatabase(token string) (int, error) {
	//Take connect form database
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return 0, utility.ErrorCreate("CHECK TOKEN", err.Error())
	}
	var id int
	if err := connToDatabase.QueryRow(context.Background(), "SELECT users_id FROM token WHERE token = $1", token).Scan(&id); err != nil {
		return 0, utility.ErrorCreate("CHECK TOKEN", err.Error())
	}
	return id, nil
}

//GenKeySessionForUserInDatabase - шифрование и сохранение пользовательского токена в ключ сессии
func (db *Database) GenKeySessionForUserInDatabase(token string, ip string) (string, error) {
	key_enscrypt := utility.GenKeyForEncriptors(ip)
	encryption_session, err := utility.Encription(key_enscrypt, token)
	if err != nil {
		return "", utility.ErrorCreate("GEN KEY SESSION", err.Error())
	}
	//Take connect form database
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	if err != nil {
		return "", utility.ErrorCreate("GEN KEY SESSION", err.Error())
	}
	defer connToDatabase.Release()
	if err != nil {
		return "", utility.ErrorCreate("GEN KEY SESSION", err.Error())
	}
	//Write key and session
	if _, err = connToDatabase.Exec(context.Background(), "INSERT INTO enscrypt_session(key_enscryption,encryption_session) VALUES($1,$2)", key_enscrypt, encryption_session); err != nil {
		return "", utility.ErrorCreate("GEN KEY SESSION WRITE TOKEN", err.Error())
	}

	return encryption_session, nil
}

//GetTokenFromKeySessionInDatabase - получение токена из ключа сессии
func (db *Database) GetTokenFromKeySessionInDatabase(encryption_session string, ip string) (string, error) {
	var key_word string
	key_enscrypt := utility.GenKeyForEncriptors(ip)
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	if err != nil {
		return "", utility.ErrorCreate("GET TOKEN FROM KEY SESSION", err.Error())
	}
	defer connToDatabase.Release()
	//Write key and session
	if err = connToDatabase.QueryRow(context.Background(), "SELECT key_enscryption FROM encryption_session WHERE encryption_session = $1", encryption_session).Scan(&key_word); err != nil {
		return "", utility.ErrorCreate("GEN TOKEN FROM KEY SESSION", err.Error())
	}
	if key_enscrypt != key_word {
		return "", utility.ErrorCreate("GEN TOKEN FROM KEY SESSION", "Ключ сессии устарел или не работает")
	}
	token, err := utility.Decrypt(key_word, encryption_session)
	if err != nil {
		return "", utility.ErrorCreate("GEN TOKEN FROM KEY SESSION", err.Error())
	}
	return token, nil
}
