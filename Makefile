#Please start as root!
version = v0.0.1

docker:
	docker build -t timetabler-man:$(version) .
	docker build -t postgres-test documents/
	echo "Success create docker - timetabler-man:$(version)"

crean:
	docker rmi -f timetabler-man:$(version)
	docker rmi -f postgres-test
	echo "Success delete docker - timetabler-man:$(version)"

run:
	docker run -p 5432:5432 -e POSTGRES_USER=admin -e POSTGRES_PASSWORD=admin -e POSTGRES_DB=timedb -e PGDATA=/var/lib/postgresql/data/pgdata -d postgres-test
	docker run -p 9001:9070 --add-host="database.scar-incom.ex:192.168.1.201" -d timetabler-man:$(version)
#run --name timetabler-test:$(version) -p 9001:9070 -p 5432:5432 -e POSTGRES_USER=habrpguser -e POSTGRES_PASSWORD=pgpwd4habr -e POSTGRES_DB=habrdb -e PGDATA=/var/lib/postgresql/data/pgdata -d -v "/absolute/path/to/directory-with-data":/var/lib/postgresql/data -v "/absolute/path/to/directory-with-init-scripts":/docker-entrypoint-initdb.d timetabler-man:$(version)