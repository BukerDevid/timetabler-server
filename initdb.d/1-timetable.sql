-- CREATE DATABASE 
-- 1. users                 - пользователи
-- 2. course                - курсы (subject)
-- 3. chapter               - главы (главы)
-- 4. progress              - прогресс прохождения занятий
-- 5. articles              - статьи
-- 6. admins                - администратор
-- 7. students              - студенты
-- 8. teachers              - преподаватели
-- 9. student_group         - группы
-- 10. cons_student_group   - объеденение студентов и группы
-- 11. token                - токен сессии

-- DROP DATABASE 
-- users
------------------------------------------
-- 'id' - индентификатор пользователя*
-- 'phone' - телефон пользователя*
-- 'password' - пароль пользователя*
-- 'firstname' - имя пользователя*
-- 'secondname' - фамилия пользователя*
-- 'middlename' - отчество пользователя
-- 'age' - возраст пользователя
-- 'mail' - адрес электронной почты
------------------------------------------
-- DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users (
  id SERIAL PRIMARY KEY NOT NULL,
  phone VARCHAR(11) NOT NULL,
  pwd VARCHAR(128) NOT NULL,
  firstname VARCHAR(30) NOT NULL,
  secondname VARCHAR(30) NOT NULL,
  middlename VARCHAR(30),
  birthday TIMESTAMP,
  mail VARCHAR(40),
  UNIQUE(phone)
);

-- admins
------------------------------------------
-- 'id' - индентификатор администратора*
------------------------------------------
-- DROP TABLE IF EXISTS admins CASCADE;
CREATE TABLE admins (
  id INTEGER PRIMARY KEY NOT NULL REFERENCES users(id) ON DELETE CASCADE
);

-- students
------------------------------------------
-- 'id' - индентификатор студента*
------------------------------------------
-- DROP TABLE IF EXISTS students CASCADE;
CREATE TABLE students (
  id INTEGER PRIMARY KEY NOT NULL REFERENCES users(id) ON DELETE CASCADE
);

-- teacher
------------------------------------------
-- 'id' - индентификатор преподавателя*
------------------------------------------
-- DROP TABLE IF EXISTS teachers CASCADE;
CREATE TABLE teachers (
  id INTEGER PRIMARY KEY NOT NULL REFERENCES users(id) ON DELETE CASCADE
);

-- enrollee
------------------------------------------
-- 'id' - индентификатор преподавателя*
------------------------------------------
-- DROP TABLE IF EXISTS enrollee CASCADE;
CREATE TABLE enrollee (
  id INTEGER PRIMARY KEY NOT NULL REFERENCES users(id) ON DELETE CASCADE
);

-- token
------------------------------------------
--'token'-токен*
--'user_id'- Идентификатор пользователя*
--'ip'- IP Адрес*
--'device'- Устройство с которого заходил пользователя*
------------------------------------------
-- DROP TABLE IF EXISTS token CASCADE;
CREATE TABLE token (
  token VARCHAR(128) PRIMARY KEY,
  users_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  ip VARCHAR(22) NOT NULL,
  device VARCHAR(64) NOT NULL,
  UNIQUE(token)
);

-- enscrypt_session
------------------------------------------
--'id'- Идентификатор записи*
--'key_session'- Секретное слово*
--'key_enscryption'- IP Адрес*
------------------------------------------
-- DROP TABLE IF EXISTS enscrypt_session CASCADE;
CREATE TABLE enscrypt_session (
  id SERIAL PRIMARY KEY,
  key_enscryption TEXT NOT NULL,
  encryption_session TEXT NOT NULL,
  UNIQUE(key_enscryption,encryption_session)
);

-- speciality
------------------------------------------
--'id'- Идентификатор записи*
--'code_speciality'- Код специальности*
--'type_speciality'- Имя*
--'description_speciality'- Описание*
------------------------------------------
-- DROP TABLE IF EXISTS speciality CASCADE;
CREATE TABLE speciality (
  id SERIAL PRIMARY KEY,
  code_speciality VARCHAR(11) NOT NULL,
  type_speciality VARCHAR(255) NOT NULL,
  description_speciality VARCHAR(255)
);

-- type_lesson
------------------------------------------
--'id'- Идентификатор записи*
--'teory'- Теория*
--'practice'- Практика*
--'laboratory'- Лабораторная*
--'consultation'- Консультация*
--'exam'- Экзамен*
------------------------------------------
-- DROP TABLE IF EXISTS type_lesson CASCADE;
CREATE TABLE type_lesson (
  id SERIAL PRIMARY KEY,
  name_lesson VARCHAR(30) NOT NULL,
  description_lession TEXT,
  attestation BOOLEAN NOT NULL,
  UNIQUE(name_lesson)
);

-- timing
------------------------------------------
--'id'- Идентификатор записи*
--'teory'- Теория*
--'practice'- Практика*
--'laboratory'- Лабораторная*
--'consultation'- Консультация*
--'exam'- Экзамен*
--'all_time'- Всего времени*
------------------------------------------
-- DROP TABLE IF EXISTS timing CASCADE;
CREATE TABLE timing (
  id SERIAL PRIMARY KEY,
  maximal INTEGER NOT NULL,
  selfwork INTEGER NOT NULL,
  all_time INTEGER NOT NULL,
  teory INTEGER NOT NULL,
  laboratory INTEGER NOT NULL,
  practice INTEGER NOT NULL,
  course_work INTEGER NOT NULL,
  consultation INTEGER NOT NULL,
  exam INTEGER NOT NULL
);

-- subject_lesson
------------------------------------------
--'id'- Идентификатор записи*
--'name_subject'- Название предмета*
--'timing_id'- Идентификатор времени*
------------------------------------------
-- DROP TABLE IF EXISTS subject_lesson CASCADE;
CREATE TABLE subject_lesson (
  id SERIAL PRIMARY KEY,
  year_create TIMESTAMP NOT NULL,
  code_subject VARCHAR(12) NOT NULL,
  name_subject VARCHAR(128) NOT NULL,
  -- TODO: add group
  course INTEGER NOT NULL,
  semester INTEGER NOT NULL, 
  timing_id INTEGER NOT NULL REFERENCES timing(id) ON DELETE CASCADE,
  speciality_id INTEGER NOT NULL REFERENCES speciality(id) ON DELETE CASCADE,
  attestation INTEGER NOT NULL REFERENCES type_lesson(id) ON DELETE CASCADE
);

-- chapter_lesson
------------------------------------------
--'id'- Идентификатор записи*
--'subject_chapter'- Идентификатор предмета*
--'name_chapter'- Название главы*
------------------------------------------
-- DROP TABLE IF EXISTS chapter_lesson CASCADE;
CREATE TABLE chapter_lesson (
  id SERIAL PRIMARY KEY,
  subject_chapter INTEGER NOT NULL REFERENCES subject_lesson(id) ON DELETE CASCADE,
  name_chapter VARCHAR(128) NOT NULL
);

-- topic
------------------------------------------
--'id'- Идентификатор записи*
--'chapter_id'- Идентификатор глава*
--'name_topic'- Название*
--'time_topic'- Время*
--'type_lesson_id'- Идентификатор типа урока*
------------------------------------------
-- DROP TABLE IF EXISTS topic CASCADE;
CREATE TABLE topic (
  id SERIAL PRIMARY KEY,
  chapter_id INTEGER NOT NULL REFERENCES chapter_lesson(id) ON DELETE CASCADE,
  name_topic TEXT NOT NULL,
  time_topic INTEGER NOT NULL,
  type_lesson_id INTEGER NOT NULL REFERENCES type_lesson(id) ON DELETE CASCADE
);

-- type_lerning
------------------------------------------
--'id'- Идентификатор записи*
--'type_learning'- Тип обучения*
--'description_learning'- Описание*
------------------------------------------
-- DROP TABLE IF EXISTS type_lerning CASCADE;
CREATE TABLE type_lerning (
  id SERIAL PRIMARY KEY,
  type_learning VARCHAR(25) NOT NULL,
  description_learning VARCHAR(128) NOT NULL
);

-- groups
------------------------------------------
--'id'- Идентификатор записи*
--'number_group'- Номер*
--'date_formation'- Дата формирования*
--'status_group'- Статус группы*
--'type_learning_id'- Тип обучения*
--'speciality_id'- Специальность*
------------------------------------------
-- DROP TABLE IF EXISTS groups CASCADE;
CREATE TABLE groups (
  id SERIAL PRIMARY KEY,
  number_group VARCHAR(10) NOT NULL,
  date_formation TIMESTAMP NOT NULL,
  status_group VARCHAR(128) NOT NULL,
  type_learning_id INTEGER NOT NULL REFERENCES type_lerning(id) ON DELETE CASCADE,
  speciality_id INTEGER NOT NULL REFERENCES speciality(id) ON DELETE CASCADE
);

-- consist_group
------------------------------------------
--'id'- Идентификатор записи*
--'group_id'- Идентификатор группы*
--'students_id'- Идентификатор студента*
------------------------------------------
-- DROP TABLE IF EXISTS consist_group CASCADE;
CREATE TABLE consist_group (
  id SERIAL PRIMARY KEY,
  group_id INTEGER NOT NULL REFERENCES groups(id) ON DELETE CASCADE,
  students_id INTEGER NOT NULL REFERENCES students(id) ON DELETE CASCADE
);

-- classroom
------------------------------------------
--'id'- Идентификатор записи*
--'class_number'- Номер класса*
------------------------------------------
-- DROP TABLE IF EXISTS classroom CASCADE;
CREATE TABLE classroom (
  id SERIAL PRIMARY KEY,
  class_number VARCHAR(5) NOT NULL
);

-- shedule
------------------------------------------
--'id'- Идентификатор записи*
--'class_number_id'- Идентификатор класса*
--'teachers_id'- Идентификатор преподователя*
--'topic_id'- Идентификатор предмета*
--'group_id'- Идентификатор группы*
--'time_shedule'- время*
------------------------------------------
-- DROP TABLE IF EXISTS shedule CASCADE;
CREATE TABLE shedule (
  id SERIAL PRIMARY KEY,
  class_number_id INTEGER NOT NULL REFERENCES classroom(id) ON DELETE CASCADE,
  teachers_id INTEGER NOT NULL REFERENCES teachers(id) ON DELETE CASCADE,
  group_id INTEGER NOT NULL REFERENCES groups(id) ON DELETE CASCADE,
  time_shedule TIMESTAMP NOT NULL
);

-- shedule
------------------------------------------
--'id'- Идентификатор записи*
--'topic_id'- Идентификатор предмета*
------------------------------------------
-- DROP TABLE IF EXISTS lesson CASCADE;
CREATE TABLE lesson (
  id SERIAL PRIMARY KEY,
  topic_id INTEGER NOT NULL REFERENCES topic(id) ON DELETE CASCADE 
);

-- type_grade
------------------------------------------
--'id'- Идентификатор записи*
--'description_grade'- Номер класса*
------------------------------------------
-- DROP TABLE IF EXISTS type_grade CASCADE;
CREATE TABLE type_grade (
  id VARCHAR(20) PRIMARY KEY,
  description_grade VARCHAR(255) NOT NULL
);

-- miss_class
------------------------------------------
--'id'- Идентификатор записи*
--'students_id'- Идентификатор студента*
--'cause'- Причина отсутствия*
--'from_date'- Дата с*
--'to_date'- Дата до*
------------------------------------------
-- DROP TABLE IF EXISTS miss_class CASCADE;
CREATE TABLE miss_class (
  id SERIAL PRIMARY KEY,
  students_id INTEGER NOT NULL REFERENCES students(id) ON DELETE CASCADE,
  cause VARCHAR(20),
  from_date TIMESTAMP,
  to_date TIMESTAMP
);

-- grade_person
------------------------------------------
--'id'- Идентификатор записи*
--'miss_class_id'- Идентификатор пропустившиие урок*
--'students_id'- Идентификатор студента*
--'teachers_id'- Идентификатор преподователя*
--'leson'- Урок*
--'type_grade_id'- Идентификатор типа обучения*
------------------------------------------
-- DROP TABLE IF EXISTS grade_person CASCADE;
CREATE TABLE grade_person (
  id SERIAL PRIMARY KEY,
  miss_class_id INTEGER NOT NULL REFERENCES miss_class(id) ON DELETE CASCADE,
  students_id INTEGER NOT NULL REFERENCES students(id) ON DELETE CASCADE,
  teachers_id INTEGER NOT NULL REFERENCES teachers(id) ON DELETE CASCADE,
  leson INTEGER NOT NULL REFERENCES shedule(id) ON DELETE CASCADE,
  type_grade_id VARCHAR NOT NULL REFERENCES type_grade(id) ON DELETE CASCADE
);

SELECT admin, student, teacher FROM 
(SELECT CASE WHEN COUNT(*) != 0 THEN TRUE ELSE FALSE END FROM admins WHERE admins.id = 1) AS admin,
(SELECT CASE WHEN COUNT(*) != 0 THEN TRUE ELSE FALSE END FROM students WHERE students.id = 1) AS student,
(SELECT CASE WHEN COUNT(*) != 0 THEN TRUE ELSE FALSE END FROM teachers WHERE teachers.id = 1) AS teacher;


CREATE OR REPLACE FUNCTION f_set_status(id_user INTEGER, new_status TEXT) RETURNS BOOLEAN LANGUAGE plpgsql AS $$
BEGIN
  DELETE FROM enrollee WHERE id = id_user;
  IF new_status = 'admin' THEN
    INSERT INTO admins(id) VALUES (id_user);
  ELSIF new_status = 'student' THEN
    INSERT INTO student(id) VALUES (id_user);
  ELSIF new_status = 'teacher' THEN
    INSERT INTO enrollee(id) VALUES (id_user);
  END IF;
  RETURN TRUE;
END;
$$;