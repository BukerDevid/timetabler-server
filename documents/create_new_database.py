# Программа для создания подключения к БД для пользователя
# 
# Default host - database.scar-incom.ex
# Default port - 5432
# Default max pool - 50
# Default database - timedb
#  
#   {
#       "connect-postgres-database": "postgres://<user>:<password>@<host>:<port>/<database>?sslmode=disable&pool_max_conns=<max_pool>",
#       "address-server":":9070"
#   }

import paramiko 
import sys
import json

class ConnectToDatabase:
    host = 'database.scar-incom.ex'
    user = str
    pwd = str
    port = 5432  # 1024 .. 65535 
    max_pool = 50 # 2 .. 500
    database = 'timedb'
    connect_postgres_database = str
    address_server = ':9070'

    def __init__(self,host,port,max_pool,database,user,pwd):
        if host != '':
            self.host = host
        if self.port != 0:
            self.port = port
        if self.max_pool != 0:
            self.max_pool = max_pool
        if database != '':
            self.database = database
        if user == '' or pwd == '':
            print('Укажите верное имя пользователя и пароль')
            quit(1)
        self.user=user
        self.pwd=pwd
        self.create_connect()

    def create_connect(self):
        if self.port < 1024 or self.port > 65535:
            print('Неправильная конфигурация системы, требуется указывать порт в пределах 1024-65535')
            quit(1)
        
        if self.max_pool < 2 or self.max_pool > 500:
            print('Неправильная конфигурация системы, требуется указывать количество возможных подключений к БД в пределах 2-500')
            quit(1)
        if self.host == '':
            print('Неправильная конфигурация системы, адрес или имя хоста не может быть пустым')
            quit(1)
        if self.database == '':
            print('Неправильная конфигурация системы, имя базы данных не может быть пустым')
            quit(1)
        self.connect_postgres_database = 'postgres://{user}:{pwd}@{host}:{port}/{database}?sslmode=disable&pool_max_conns={max_pool}'.format(user=self.user,pwd=self.pwd,host=self.host,port=str(self.port),database=self.database,max_pool=str(self.max_pool))
    
    def create_database(self):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=self.host,username='root',password='Put1n@ebanaya',port='22')
        stdin, stdout, stderr = client.exec_command('ls -l')
        data = stdout.read() + stderr.read()
        print(data)
        client.close()


# if sys.argv[1] == 'default':
#     with open('./config.json','w',encoding='utf-8') as config_file:
#         data = {
#             'connect-postgres-database': 'postgres://admin:admin@database.scar-incom.ex:5432/ondb?sslmode=disable&pool_max_conns=50',
#             'address-server': ':9070'
#         }
#         json.dump(data,config_file,ensure_ascii=False)
#         quit(0)

config = ConnectToDatabase(sys.argv[1],int(sys.argv[2]),int(sys.argv[3]),sys.argv[4],sys.argv[5],sys.argv[6])
with open('./config.json','w',encoding='utf-8') as config_file:
    data = {
        'connect-postgres-database': config.connect_postgres_database,
        'address-server': config.address_server
    }
    json.dump(data,config_file,ensure_ascii=False)

config.create_database()