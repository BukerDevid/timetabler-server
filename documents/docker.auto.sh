#!/bin/bash
# master-server
# ed-server
# mx-server
# vs-server
INFO_DOCKER_AUTO=`cat <<_EOF_ 
Информация по использованию doсker.auto

docker.auto [ПОЛЬЗОВАТЕЛЬ] [ОПЦИИ]

Опции:

    [Взаимодействие с севером]

    -h  (--help)          Помощь
    -r  (--run)           Запуск докера
    -u  (--update)        Обновить
    -s  (--stop)          Остановить работу докера
        (--status)        Статус сервера


    [Взаимодействие с бд] (0.0.2)

    -d  (--database)        Информация по конфигурации подключения к базе данных
    -ud (--update-database) Обновление конфигурации базы данных (все изменения в бд будут утеряны)
    -cg (--config-change)   Создать базу данных для сервера (сервер зависит от пользователя)  

    [Взаимодействие в web] (0.0.3)

    -w (--web)                  Разворачивает веб приложение (по умолчанию стабильный выпуск)
       (--web-night-release)    Развернуть тестовую версию (Возможна нестабильная работа)


Пользователи:

    vas - Vasiliy   (RELEASE branch)
    edi - Eduard    (DATABASE branch)
    max - Max       (DEVELOP branch)
    mas - Master    (MASTER branch)

Пример:

    docker.auto mas -r
_EOF_
`
run_docker(){
    echo 'Запуск докера'
    if [ "$1" == mas ]
    then
        echo 'MASTER SERVER START'
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml up -d master-database &
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml up -d master-server &
    fi
    if [ "$1" == edi ]
    then
        echo 'DATABASE SERVER START'
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml up -d ed-database &
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml up -d ed-server &
    fi
    if [ "$1" == max ]
    then
        echo 'DEVELOP SERVER START'
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml up -d mx-database &
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml up -d mx-server &
    fi
    if [ "$1" == vas ]
    then
        echo 'RELEASE SERVER START'
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml up -d vs-database &
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml up -d vs-server &
    fi
}

run_with_update(){
    echo 'Обновление и запуск докера'
    if [ "$1" == mas ]
    then
        echo 'MASTER SERVER UPDATE' 
        cd /srv/servers/master
        git pull
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml build master-server
    fi
    if [ "$1" == edi ]
    then
        echo 'DATABASE SERVER UPDATE'
        cd /srv/servers/eduard-server
        git pull
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml build ed-server
    fi
    if [ "$1" == max ]
    then
        echo 'DEVELOP SERVER UPDATE'
        cd /srv/servers/max-server
        git pull
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml build mx-server
    fi
    if [ "$1" == vas ]
    then
        echo 'RELEASE SERVER UPDATE'
        cd /srv/servers/vas-server
        git pull
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml build vs-server
    fi
}

stop_docker(){
    echo 'Остановка докера'
    if [ "$1" == mas ]
    then
        echo 'MASTER SERVER STOP' 
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml stop master-server
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml stop master-database
    fi
    if [ "$1" == edi ]
    then
        echo 'DATABASE SERVER STOP' 
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml stop ed-server
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml stop ed-database
    fi
    if [ "$1" == max ]
    then
        echo 'DEVELOP SERVER STOP' 
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml stop mx-server
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml stop mx-database
    fi
    if [ "$1" == vas ]
    then
        echo 'RELEASE SERVER STOP' 
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml stop vs-server
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml stop vs-database
    fi
}

status_docker(){
    echo 'Статус сервера'
    if [ "$1" == mas ]
    then
        echo 'MASTER SERVER STATUS' 
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml ps master-server
    fi
    if [ "$1" == edi ]
    then
        echo 'DATABASE SERVER STATUS' 
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml ps ed-server
    fi
    if [ "$1" == max ]
    then
        echo 'DEVELOP SERVER STATUS' 
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml ps mx-server
    fi
    if [ "$1" == vas ]
    then
        echo 'RELEASE SERVER STATUS' 
        /usr/bin/docker-compose -f /srv/servers/docker-compose.yml ps vs-server
    fi
}

config_docker_for_connect_to_database(){
    echo 'Конфигурация серверов для подключения к базе данных'
    if [ "$1" == mas ]
    then
        echo 'MASTER SERVER STATUS' 
        echo `cat /srv/servers/master/documents/config.json`
    fi
    if [ "$1" == edi ]
    then
        echo 'DATABASE SERVER STATUS' 
        echo `cat /srv/servers/eduard-server/documents/config.json`
    fi
    if [ "$1" == max ]
    then
        echo 'DEVELOP SERVER STATUS' 
        echo `cat /srv/servers/max-server/documents/config.json`
    fi
    if [ "$1" == vas ]
    then
        echo 'RELEASE SERVER STATUS' 
        echo `cat /srv/servers/vas-server/documents/config.json`
    fi
}

create_new_config_for_connect_to_database_for_docker(){
    echo "При создании базы данных использовать данные по умолчанию? (Y-yes, other for no): "
    read defaultData
    if [ "$defaultData" == 'Y' ]
    then
        hostname=database.scar-incom.ex 
        port=5432 
        max_pool=50 
        database_name=timedb 
        user_name=admin 
        user_pwd=admin
        python3 ./create_new_config.py $hostname $port $max_pool $database_name $user_name $user_pwd
    else
        echo "Адрес или имя сервера БД (default: database.scar-incom.ex): "
        read hostname 
        if [ -z "$hostname" ]
        then
            hostname=database.scar-incom.ex
        fi
        echo "Порт сервера (default: 5432): "
        read port 
        if [ -z "$port" ]
        then
            port=5432
        fi
        echo "Максимальное количество подключений к БД от 2 до 500 (default: 50): "
        read max_pool 
        if [ -z "$max_pool" ]
        then
            max_pool=50
        fi
        echo "Имя схемы БД (default: timedb): "
        read database_name 
        if [ -z "$database_name" ]
        then
            database_name=timedb
        fi
        echo "Пользователь (default: admin): "
        read user_name 
        if [ -z "$user_name" ]
        then
            user_name=admin
        fi
        echo "Пароль (default: admin): "
        read user_pwd 
        if [ -z "$user_pwd" ]
        then
            user_pwd=admin
        fi
        python3 ./create_new_config.py $hostname $port $max_pool $database_name $user_name $user_pwd
    fi
}

create_new_database(){
    echo "При создании базы данных использовать данные по умолчанию? (Y-yes, other for no): "
    read defaultData
    if [ "$defaultData" == 'Y' ]
    then
        hostname=database.scar-incom.ex 
        port=5432 
        max_pool=50 
        database_name=timedb 
        user_name=admin 
        user_pwd=admin
        python3 ./create_new_config.py $hostname $port $max_pool $database_name $user_name $user_pwd
    else
        echo "Адрес или имя сервера БД (default: database.scar-incom.ex): "
        read hostname 
        if [ -z "$hostname" ]
        then
            hostname=database.scar-incom.ex
        fi
        echo "Порт сервера (default: 5432): "
        read port 
        if [ -z "$port" ]
        then
            port=5432
        fi
        echo "Максимальное количество подключений к БД от 2 до 500 (default: 50): "
        read max_pool 
        if [ -z "$max_pool" ]
        then
            max_pool=50
        fi
        echo "Имя схемы БД (default: timedb): "
        read database_name 
        if [ -z "$database_name" ]
        then
            database_name=timedb
        fi
        echo "Пользователь (default: admin): "
        read user_name 
        if [ -z "$user_name" ]
        then
            user_name=admin
        fi
        echo "Пароль (default: admin): "
        read user_pwd 
        if [ -z "$user_pwd" ]
        then
            user_pwd=admin
        fi
        python3 ./create_new_config.py $hostname $port $max_pool $database_name $user_name $user_pwd
        python3 ./create_database.py $hostname $port $max_pool $database_name $user_name $user_pwd
    fi
}

if [ -n "$1" ] || [ -n "$2" ]
then
    if [ "$2" == -h ] || [ "$2" == --help ]
    then
        echo "$INFO_DOCKER_AUTO"
    fi
    if [ $1 != vas ] && [ $1 != edi ] && [ $1 != max ] && [ $1 != mas ]
    then
        echo "$INFO_DOCKER_AUTO"
    fi 
    if [ "$2" == -r ] || [ "$2" == --run ]
    then
        run_docker $1
    fi
    if [ "$2" == -u ] || [ "$2" == --update ]
    then
        run_with_update $1
    fi
    if [ "$2" == -s ] || [ "$2" == --stop ]
    then
        stop_docker $1
    fi
    if [ "$2" == --status ]
    then
       status_docker $1
    fi
    if [ "$2" == -d ] || [ "$2" == --database ]
    then
        create_new_config_for_connect_to_database_for_docker $1
    fi
    if [ "$2" == -ud ] || [ "$2" == --update-database ]
    then
        create_new_database_for_docker $1
    fi
else
    echo "$INFO_DOCKER_AUTO"
fi